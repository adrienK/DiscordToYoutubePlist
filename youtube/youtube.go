package youtube

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

type Client struct {
	API    string
	ID     string
	Secret string
}

var auth *oauth2.Token
var client Client

func TokenTimeLeft(offSet time.Duration) time.Duration {
	n := time.Now()

	t := auth.Expiry
	t = t.Add(-offSet)

	if n.After(t) {
		return 0 * time.Second
	}

	rd := t.Sub(n)
	if rd <= 0 {
		rd = offSet
	}

	return rd
}

func TokenRenew() {
	_ = auth.RefreshToken
}

func getToken() string {
	return auth.AccessToken
}

func getAuth() string {
	return fmt.Sprintf("%s %s", auth.TokenType, auth.AccessToken)
}

func Init(c Client) {
	var err error
	client = c
	ctx := context.Background()

	conf := &oauth2.Config{
		ClientID:     c.ID,
		ClientSecret: c.Secret,
		Endpoint:     google.Endpoint,
		RedirectURL:  "urn:ietf:wg:oauth:2.0:oob",
		Scopes:       []string{"https://www.googleapis.com/auth/youtube"},
	}

	url := conf.AuthCodeURL("state", oauth2.AccessTypeOffline)
	fmt.Printf("Visit the URL for the auth dialog: %v\n", url)

	var code string
	fmt.Printf("Past Oauth code here: ")
	if _, err := fmt.Scan(&code); err != nil {
		log.Fatal(err)
	}

	httpClient := &http.Client{Timeout: 2 * time.Second}
	ctx = context.WithValue(ctx, oauth2.HTTPClient, httpClient)

	auth, err = conf.Exchange(ctx, code)
	if err != nil {
		log.Fatal(err)
	}
}

func AddToPList(playlistId string, videoID []string) ([]string, error) {
	var errList []string

	c := &http.Client{}
	r, err := http.NewRequest(
		"POST",
		fmt.Sprintf(
			"https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&key=%s",
			client.API,
		),
		nil,
	)
	if err != nil {
		return errList, err
	}

	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("Authorization", getAuth())

	body := `{
		"snippet": {
			"playlistId": "%s",
			"position": 0,
			"resourceId": {
				"kind": "youtube#video",
				"videoId": "%s"
			}
		}
	}`

	for _, vid := range videoID {
		r.Body = ioutil.NopCloser(
			strings.NewReader(
				fmt.Sprintf(body, playlistId, vid),
			),
		)

		rr, err := c.Do(r)
		if err != nil {
			errList = append(errList, vid)
			fmt.Println(err)
			continue
		}

		if rr.StatusCode != 200 {
			errList = append(errList, vid)
			a, _ := ioutil.ReadAll(rr.Body)
			fmt.Printf("\n%s\n%s\n", rr.Status, a)
		}
	}

	return errList, nil
}
