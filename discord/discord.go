package discord

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

type dapiType struct {
	authType string
	authID   string
	chanID   string
}

type MessageApi []struct {
	ID        string      `json:"id"`
	Content   string      `json:"content"`
	Embeds    []Embeds    `json:"embeds,omitempty"`
	Reactions []Reactions `json:"reactions,omitempty"`
}

type Embeds struct {
	Type     string `json:"type"`
	URL      string `json:"url"`
	Provider struct {
		Name string `json:"name"`
	} `json:"provider"`
}

type Reactions struct {
	Me bool `json:"me"`
}

var uapi string
var dapi dapiType

// Init discord api base conf
func Init(authType string, authID string, chanID string) {
	uapi = "https://discordapp.com/api"

	dapi.chanID = chanID
	dapi.authID = authID

	switch authType {
	case "Bearer", "Bot":
		dapi.authType = fmt.Sprintf("%s ", authType)
	case "User":
		dapi.authType = ""
	default:
		log.Fatalf("[Discord API] Unable to use Auth type: %s", authType)
	}

	apiAuthCheck()
}

func apiAuthCheck() {
	url := fmt.Sprintf("%s/users/@me", uapi)
	auth := fmt.Sprintf("%s%s", dapi.authType, dapi.authID)
	code, body := sendAPI(url, auth, "GET")

	if code != 200 {
		log.Fatalf("Error to inital request API [%d] %s", code, body)
	}
}

// Messages get last messages group
func Messages(lastID string) MessageApi {
	var msg MessageApi

	if lastID != "" {
		lastID = fmt.Sprintf("&after=%s", lastID)
	}

	url := fmt.Sprintf("%s/channels/%s/messages?limit=100%s", uapi, dapi.chanID, lastID)
	auth := fmt.Sprintf("%s%s", dapi.authType, dapi.authID)
	code, body := sendAPI(url, auth, "GET")

	if code == 200 {
		json.Unmarshal(body, &msg)
	} else {
		log.Printf("Error to channel request [%d] %s", code, body)
	}

	return msg
}

func ReactMsg(msgID string, emoji string) {
	urlRq := fmt.Sprintf(
		"%s/channels/%s/messages/%s/reactions/%s/@me",
		uapi, dapi.chanID, msgID, url.QueryEscape(emoji),
	)
	auth := fmt.Sprintf("%s%s", dapi.authType, dapi.authID)

	sendAPI(urlRq, auth, "PUT")
}

func RemoveReactMsg(msgID string, emoji string) {
	urlRq := fmt.Sprintf(
		"%s/channels/%s/messages/%s/reactions/%s/@me",
		uapi, dapi.chanID, msgID, url.QueryEscape(emoji),
	)
	auth := fmt.Sprintf("%s%s", dapi.authType, dapi.authID)

	sendAPI(urlRq, auth, "DELETE")
}

func sendAPI(url string, auth string, meth string) (int, []byte) {
	client := http.Client{}

	req, _ := http.NewRequest(meth, url, nil)
	req.Header.Set("Authorization", auth)
	req.URL.RequestURI()
	time.Sleep(1 * time.Second)

	res, _ := client.Do(req)
	body, _ := ioutil.ReadAll(res.Body)

	return res.StatusCode, body
}
