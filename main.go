package main

import (
	"fmt"
	"strings"
	"time"

	"git.iglou.eu/adrien/DiscordToYoutubePlist/discord"
	"git.iglou.eu/adrien/DiscordToYoutubePlist/youtube"
)

type ytbMsg struct {
	msgID string
	ytbID string
}

var playlistId string

func init() {
	playlistId = "PLBmTzW9VJX1kYervsGSRtyqokRTVz5KuZ"

	youtube.Init(youtube.Client{
		"",
		"",
		"",
	})

	discord.Init(
		"User",
		"",
		"",
	)
}

func main() {
	var lastMsg string

	for {
		var yList []ytbMsg

		buildPlist(discord.Messages(lastMsg), &yList)
		pushPlist(yList)

		if len(yList) > 0 {
			lastMsg = yList[0].msgID
		}

		time.Sleep(
			youtube.TokenTimeLeft(10 * time.Minute),
		)

		youtube.TokenRenew()
	}
}

func buildPlist(msg discord.MessageApi, yL *[]ytbMsg) {
	var yList []ytbMsg

	for _, v := range msg {
		if len(v.Reactions) > 0 && isMyReact(v.Reactions) {
			continue
		}

		if len(v.Embeds) < 1 {
			continue
		}

		yList = append(yList, yExtract(v.ID, v.Embeds)...)
	}

	*yL = yList
}

func isMyReact(r []discord.Reactions) bool {
	for _, v := range r {
		if v.Me {
			return true
		}
	}

	return false
}

func yExtract(i string, e []discord.Embeds) []ytbMsg {
	var yList []ytbMsg

	for _, v := range e {
		if v.Type != "video" {
			continue
		}

		if v.Provider.Name != "YouTube" {
			continue
		}

		s := strings.Split(v.URL, "/")
		s = strings.Split(s[len(s)-1], "v=")
		s = strings.Split(s[len(s)-1], "&")

		y := ytbMsg{msgID: i, ytbID: s[0]}

		yList = append(yList, y)
	}

	return yList
}

func pushPlist(ytm []ytbMsg) {
	var ytList []string

	for _, v := range ytm {
		fmt.Println("Entry", v)
		discord.ReactMsg(v.msgID, "💾")
		ytList = append(ytList, v.ytbID)
	}

	ytListErr, err := youtube.AddToPList(playlistId, ytList)
	if err != nil {
		fmt.Println(err)
	}

	for _, v := range ytListErr {
		discord.RemoveReactMsg(
			vidToMsgID(ytm, v),
			"💾",
		)
	}
}

func vidToMsgID(ytm []ytbMsg, vid string) string {
	for _, v := range ytm {
		if v.ytbID == vid {
			return v.msgID
		}
	}

	return ""
}
